# Flatbuffers definitions

These are the flatbuffers message definitions we have used with [Veneris](http://pcacribia.upct.es/veneris)
Messages have been compiled with flatbuffers 1.10.0. See [flatbuffers](https://google.github.io/flatbuffers/)

