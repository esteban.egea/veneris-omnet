// automatically generated by the FlatBuffers compiler, do not modify


#ifndef FLATBUFFERS_GENERATED_REGISTERGAIN_VENERIS_COMMUNICATIONS_H_
#define FLATBUFFERS_GENERATED_REGISTERGAIN_VENERIS_COMMUNICATIONS_H_

#include "flatbuffers/flatbuffers.h"

#include "BaseTypes_generated.h"

namespace Veneris {
namespace Communications {

struct RegisterGain;
struct RegisterGainBuilder;

struct RegisterGain FLATBUFFERS_FINAL_CLASS : private flatbuffers::Table {
  typedef RegisterGainBuilder Builder;
  enum FlatBuffersVTableOffset FLATBUFFERS_VTABLE_UNDERLYING_TYPE {
    VT_PATH = 4
  };
  const flatbuffers::Vector<int8_t> *path() const {
    return GetPointer<const flatbuffers::Vector<int8_t> *>(VT_PATH);
  }
  bool Verify(flatbuffers::Verifier &verifier) const {
    return VerifyTableStart(verifier) &&
           VerifyOffset(verifier, VT_PATH) &&
           verifier.VerifyVector(path()) &&
           verifier.EndTable();
  }
};

struct RegisterGainBuilder {
  typedef RegisterGain Table;
  flatbuffers::FlatBufferBuilder &fbb_;
  flatbuffers::uoffset_t start_;
  void add_path(flatbuffers::Offset<flatbuffers::Vector<int8_t>> path) {
    fbb_.AddOffset(RegisterGain::VT_PATH, path);
  }
  explicit RegisterGainBuilder(flatbuffers::FlatBufferBuilder &_fbb)
        : fbb_(_fbb) {
    start_ = fbb_.StartTable();
  }
  flatbuffers::Offset<RegisterGain> Finish() {
    const auto end = fbb_.EndTable(start_);
    auto o = flatbuffers::Offset<RegisterGain>(end);
    return o;
  }
};

inline flatbuffers::Offset<RegisterGain> CreateRegisterGain(
    flatbuffers::FlatBufferBuilder &_fbb,
    flatbuffers::Offset<flatbuffers::Vector<int8_t>> path = 0) {
  RegisterGainBuilder builder_(_fbb);
  builder_.add_path(path);
  return builder_.Finish();
}

inline flatbuffers::Offset<RegisterGain> CreateRegisterGainDirect(
    flatbuffers::FlatBufferBuilder &_fbb,
    const std::vector<int8_t> *path = nullptr) {
  auto path__ = path ? _fbb.CreateVector<int8_t>(*path) : 0;
  return Veneris::Communications::CreateRegisterGain(
      _fbb,
      path__);
}

inline const Veneris::Communications::RegisterGain *GetRegisterGain(const void *buf) {
  return flatbuffers::GetRoot<Veneris::Communications::RegisterGain>(buf);
}

inline const Veneris::Communications::RegisterGain *GetSizePrefixedRegisterGain(const void *buf) {
  return flatbuffers::GetSizePrefixedRoot<Veneris::Communications::RegisterGain>(buf);
}

inline bool VerifyRegisterGainBuffer(
    flatbuffers::Verifier &verifier) {
  return verifier.VerifyBuffer<Veneris::Communications::RegisterGain>(nullptr);
}

inline bool VerifySizePrefixedRegisterGainBuffer(
    flatbuffers::Verifier &verifier) {
  return verifier.VerifySizePrefixedBuffer<Veneris::Communications::RegisterGain>(nullptr);
}

inline void FinishRegisterGainBuffer(
    flatbuffers::FlatBufferBuilder &fbb,
    flatbuffers::Offset<Veneris::Communications::RegisterGain> root) {
  fbb.Finish(root);
}

inline void FinishSizePrefixedRegisterGainBuffer(
    flatbuffers::FlatBufferBuilder &fbb,
    flatbuffers::Offset<Veneris::Communications::RegisterGain> root) {
  fbb.FinishSizePrefixed(root);
}

}  // namespace Communications
}  // namespace Veneris

#endif  // FLATBUFFERS_GENERATED_REGISTERGAIN_VENERIS_COMMUNICATIONS_H_
